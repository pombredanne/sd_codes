#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include "gf_complete.h"

#define talloc(type, num) (type *) malloc(sizeof(type)*(num))

void usage(char *s)
{
  fprintf(stderr, "usage: general_construction n m s r w x_0 y_0 x_1 y_1 .. x_{m+s-1} y_{m+s-1}\n");
  fprintf(stderr, "       produces the parity check matrix for the general construction.\n");
  if (s != NULL) fprintf(stderr, "%s\n", s);
  exit(1);
}

uint32_t alpha_to_the_x(gf_t *gf, uint32_t x)
{
  uint32_t prod, exp;

  prod = 1;
  exp = 2;
  while (x != 0) {
    if (x&1) prod = gf->multiply.w32(gf, prod, exp);
    x >>= 1;
    exp = gf->multiply.w32(gf, exp, exp);
  }
  return prod;
}

void print_matrix(int *m, int rows, int cols, int w)
{
  int i, j;
  int fw;
  char s[30];
  unsigned int w2;

  if (w == 32) {
    fw = 10;
  } else {
    w2 = (1 << w);
    sprintf(s, "%u", w2-1);
    fw = strlen(s);
  }

  for (i = 0; i < rows; i++) {
    for (j = 0; j < cols; j++) {
      if (j != 0) printf(" ");
      printf("%*u", fw, m[i*cols+j]);
    }
    printf("\n");
  }
}

int main(int argc, char **argv)
{
  int i, j, n, m, r, s, w;
  int *matrix;
  int k, l;
  uint32_t *x_i, *y_i;
  uint64_t mod;
  int64_t c;
  int pp;
  gf_t gf_s;

  if (argc < 6) usage(NULL);
  if (sscanf(argv[1], "%d", &n) == 0 || n <= 0) usage("Bad n");
  if (sscanf(argv[2], "%d", &m) == 0 || m <= 0) usage("Bad m");
  if (sscanf(argv[3], "%d", &s) == 0 || s <= 0) usage("Bad s");
  if (sscanf(argv[4], "%d", &r) == 0 || r <= 0) usage("Bad r");
  if (sscanf(argv[5], "%d", &w) == 0 || w <= 0) usage("Bad w");
  pp = 0;
  sscanf(argv[5], "%d-%o", &i, &pp);
  if (argc != 6 + (m+s) * 2) usage("Wrong number of arguments");

  x_i = talloc(uint32_t, m+s);
  y_i = talloc(uint32_t, m+s);

  mod = 1;
  mod <<= w;
  mod -= 1;
  
  for (i = 0; i < m+s; i++) {
    if (sscanf(argv[6+i*2], "%lld", (long long int *) &c) == 0) usage("Bad x_i");
    if (c < 0) {
      c = -c;
      c %= mod;
      if (c != 0) c = mod - c;
    } else {
      c %= mod;
    }
    x_i[i] = c;
    if (sscanf(argv[6+i*2+1], "%lld", (long long int *) &c) == 0) usage("Bad y_i");
    if (c < 0) {
      c = -c;
      c %= mod;
      c = mod - c;
    } else {
      c %= mod;
    }
    y_i[i] = c;
  }

  matrix = talloc(int, n*r*(m*r+s));
  bzero(matrix, sizeof(int)*n*r*(m*r+s));

  if (pp == 0) {
    if (w == 32) {      
      if (gf_init_hard(&gf_s, w, GF_MULT_SPLIT_TABLE, GF_REGION_DEFAULT, GF_DIVIDE_EUCLID, 0, 8, 8, 
             NULL, NULL) == 0) {
        fprintf(stderr, "Couldn't open GF\n");
        exit(1);
      }
    } else {
      if (gf_init_easy(&gf_s, w) == 0) {
        fprintf(stderr, "Couldn't open GF\n");
        exit(1);
      }
    }
  } else {
    if (gf_init_hard(&gf_s, w, GF_MULT_SHIFT, GF_REGION_DEFAULT,
                     GF_DIVIDE_EUCLID, pp, 0, 0, NULL, NULL) == 0) {
      fprintf(stderr, "Couldn't open GF\n");
      exit(1);
    }
  }

  for (i = 0; i < m; i++) {
    for (j = 0; j < n; j++) {
      for (k = 0; k < r; k++) matrix[(k*m+i)*(r*n)+(k*n)+j] = alpha_to_the_x(&gf_s, k*x_i[i]*n+j*y_i[i]);
    }
  }

  for (i = 0; i < s; i++) {
    for (j = 0; j < n*r; j++) {
      matrix[(m*r+i)*n*r+j] = alpha_to_the_x(&gf_s, (j/n)*x_i[i+m]*n+(j%n)*y_i[i+m]);
    }
  }

  print_matrix(matrix, m*r+s, n*r, w);
  exit(0);
} 
