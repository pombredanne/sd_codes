<title>Open Source Encoder and Decoder for SD Erasure Codes - Revision 2.0</title>

<center>
<h1>
Open Source Encoder and Decoder for SD Erasure Codes - Revision 2.0</h1>
<h2><a href=http://web.eecs.utk.edu/~plank/>James S. Plank</a></h2>
Technical Report UT-CS-13-707<br>
EECS Department<br>
University of Tennessee<p>
May, 2013
<p>
<a href=http://web.eecs.utk.edu/~plank/plank/papers/CS-13-707.html>http://web.eecs.utk.edu/~plank/plank/papers/CS-13-707.html</a><br>
<p>
The source code is available on Bitbucket:
<a href=https://bitbucket.org/jimplank/sd_codes>https://bitbucket.org/jimplank/sd_codes</a>.<br>
This manual is part of that repository, in <b>Manual.html</b>.
</center>

<hr>
<h2>Citation Information</h3>

<pre>
.techreport     p:13:ossd2
author          J. S. Plank
title           Open Source Encoder and Decoder for SD Erasure Codes - Revision 2.0
institution     University of Tennessee
month           May
year            2013
number          UT-CS-13-707
where           http://web.eecs.utk.edu/~plank/plank/papers/CS-13-707.html

@TECHREPORT{p:13:ossd2,
        author = "J. S. Plank",
        title = "Open Source Encoder and Decoder for {SD} Erasure Codes - Revision 2.0",
        institution = "University of Tennessee",
        month = "May",
        year = "2013",
        number = "UT-CS-13-707",
        where = "http://web.eecs.utk.edu/~plank/plank/papers/CS-13-707.html"
}
</pre>
<hr>
<h2>If You Use This Code</h2>

Please send me an email to let me know how it goes.  Or send me an email just to let
me know you are using the library.  One of the ways in which I
am evaluated both internally and externally is by the impact of my work, and if
you have found this library and/or this document useful,
I would like to be able to document it.
Please send mail to <a href=mailto:plank@cs.utk.edu>plank@cs.utk.edu</a>.  
Please send bug reports to that address as well.

<p>
The library itself is protected by the New BSD License.  

<hr>
<h2>Revision 1.0 and its Relationship to Revision 2.0</h2>

Following the FAST paper, Mario and I continued to work on the SD codes, and came
up with a slightly different formulation, which yielded a wider variety of 
codes.  In the FAST paper, the parity check matrix was specified by <i>m+s</i> 
coefficients, <i>a<sub>0</sub></i> through <i>a<sub>m+s-1</sub></i>, and when 
a coefficient was used in column <i>j</i> of the parity check matrix, it was
raised to the <i>j-th</i> power in <i>GF(2<sup>w</sup>)</i>.
<p>
In the follow-on work, the parity check matrix is specified by two sets of <i>m+s</i>
numbers, which we label <i>X</i> and <i>Y</i>.  When a coefficient is used 
in column <i>j</i> of the parity check matrix, its value is:
<p>
<center>
<i>2<sup>x<sub>i</sub>(j/n)n+y<sub>i</sub>(j%n)</sup></i>,
</center>
<p>
where division is integer division and "%" is the modulo operator.  The construction 
in the FAST paper may be expressed using this formulation as follows.  Every value in
<i>GF(2<sup>w</sup>)</i> is equal to <i>2<sup>z</sup></i> for some value of <i>z</i> 
between 0 and <i>2<sup>w</sup></i>-2.  Let <i>a<sub>i</sub></i> in the FAST formulation
be equal to <i>2<sup>z<sub>i</sub></sup></i>.  Then the sets <i>X</i> and <i>Y</i> are
defined so that 
<i>x<sub>i</sub></i> = <i>y<sub>i</sub></i> = <i>z<sub>i</sub></i>.
<p>
In revision 1.0 of this code, the encoder/decoder took the <i>a<sub>i</sub></i> coefficients
on the command line and constructed the code according to the FAST formulation.  
In revision 2.0, it takes the name of a file holding the parity 
check matrix, and I have two programs that generate parity check matrices:
<UL>
<LI> <b>fast_construction.c</b> generates it using the <i>a<sub>i</sub></i> coefficients
and the methodology from the FAST paper.
<LI> <b>general_construction.c</b> generates it using the <i>X</i> and <i>Y</i> 
coefficients.
</UL>
I will give examples later.

<hr>
<h2>Description of SD Codes</h2>

SD Codes were first presented explained in [Plank-Blaum-Hafner-2013] (references at the end of this document).  We later updated the paper, and submitted it to <i>ACM Transactions on Storage</i>.
The submission is [Plank-Blaum-2013].  This document will be updated if/when the paper
is accepted and appears.  This 
user manual assumes that you have read [Plank-Blaum-2013], and it adheres to the nomenclature
of that paper.

<hr>
<h2>What is in the repository</h2>

There are five programs in this directory.  The main one is a program called
<b>sd_codec.c</b>, which performs both encoding and decoding
using SD codes.  (Note, in revision 1.0, this was named <b>sd_code</b>.  I have
renamed it so that there are not backward compatibility issues).
The intent of this program is to be a starting point for others
to implement and use SD codes in their own systems.  It is not meant to be a general
purpose SD encoder and decoder.
<p>
The second and third programs are
<b>fast_construction.c</b> and 
<b>general_construction.c</b>.  These generate parity check matrices for <b>sd_codec.c</b>.
<p>
The fourth program is 
<b>generate_random_data_file.c</b>, which 
generates a random input file for <b>sd_codec</b>.  These input files need to be
in a specific format.  One uses <b>sd_codec</b> to encode the data in the input
file.
<p>
The last program is 
<b>erase_data.c</b>.  When you call it on the encoded 
file, it will erase data, so that <b>sd_codec</b> may decode it.  
<p>
Finally, there are two text files:
<b>FAST-Coefficients.txt</b> 
contains the <i>a<sub>i</sub></i> for FAST constructions from [Plank-Blaum-Hafner-2013].
These are valid SD code coefficients for all values of <i>n &le; 24</i>, <i>m &le; 3</i>, 
<i>s &le; 3</i> and <i>r &le; 24</i>.  
<p>
<b>TOS-Coefficients.txt</b> contains coefficients from [Plank-Blaum-2013].
There are many more constructions here for smaller values of <i>w</i>.
<p>
Finally, there are some text files that we use in examples.  They start with "example."
<hr>
<h2>gf_complete.h and gf_complete.a</h2>

Galois Field arithmetic is implemented in the library ``GF-Complete''
[Plank-Greenan-Miller-Houston-2013].    This is available on Bitbucket at
<b><a href=https://bitbucket.org/ethanmiller/gf-complete>https://bitbucket.org/ethanmiller/gf-complete</a></b>.
Download it, compile it, and then copy the files
<b>gf_complete.h</b> and <b>gf_complete.a</b>.  These are used to compile <b>sd_codec.c</b>.
<p>
If you modify this code for your own purposes, please make note of the fact that the
<i>w=16</i> and <i>w=32</i> codes make use of the "alternate mapping," which is the 
fastest implementation technique for these values of <i>w</i>.  If you are unsure of 
how to use these mappings (see the manual for GF-Complete and [Plank-Greenan-Miller-2013]), 
you may want to simply 
use the default implementations for these values of <i>w</i>, which are slower, but
more intuitive.

<hr>
<h2>Parameters and file format for sd_codec</h2>

When performing encoding and decoding, <b>sd_codec</b> makes use of the following parameters:

<p>
<center><table border=3 cellpadding=3>
<tr><td align=center>Parameter</td><td align=center>Description</td></tr>
<tr><td align=center><i>n</i></td><td align=center>The total number of disks</td></tr>
<tr><td align=center><i>m</i></td><td align=center>The number of disks devoted to coding</td></tr>
<tr><td align=center><i>s</i></td><td align=center>The number of extra blocks devoted to coding</td></tr>
<tr><td align=center><i>r</i></td><td align=center>The number of blocks per stripe on each disk (rows)</td></tr>
<tr><td align=center><i>w</i></td><td align=center>The symbol (word) size for the Galois Field</td></tr>
<tr><td align=center><i>size</i></td><td align=center>The number of bytes in a block</td></tr>
<tr><td align=center><i>H</i></td><td align=center>A <i>(mr+s)</i> by <i>(nr)</i> parity check matrix.
</table>
</center>
<p>
<b>Sd_codec</b>, <b>generate_random_data_file</b> and <b>erase_data</b>
create and manipulate files in the following format.
The files contain <i>n*r*size</i> words, which are bytes in hexadecimal.  The first <i>size</i>
words define the bytes in row 0 of disk 0.  The next <i>size</i> words define the bytes 
in row 0 of disk 1.  And so on.
<p>
To be precise, word <i>i</i> (zero indexed) in a file is byte <i>(i % size)</i> 
in row <i>(i/(size*n))</i> of disk <i>(i/size) % n</i>.  
<p>
For example, 
the file 
<b>example_6_2_2_4_data.txt</b> contains data
for an SD code with <i>n=6</i>, <i>m=2</i>, <i>s=2</i>, <i>r=4</i>, <i>size=8</i>.  
Thus, there are 24 blocks on 6 disks, four blocks per disk, eight bytes per block.
This file is formatted with eight words per line, so each block is on a separate
line:

<pre>
UNIX> <font color=darkred><b>awk '{ printf "%s  Block %2d: Disk %d, Row %d\n", $0, n, n%6, n/6; n++ }' < example_6_2_2_4_data.txt</b></font>
6e c0 0a 91 d0 f2 69 ae   Block  0: Disk 0, Row 0
ba b4 48 3b 0b 5c ea 66   Block  1: Disk 1, Row 0
83 81 c8 15 52 b9 29 7d   Block  2: Disk 2, Row 0
3a 7d 5a d4 63 29 8b 1e   Block  3: Disk 3, Row 0
00 00 00 00 00 00 00 00   Block  4: Disk 4, Row 0
00 00 00 00 00 00 00 00   Block  5: Disk 5, Row 0
bb 20 46 24 07 0d 8c 84   Block  6: Disk 0, Row 1
8b 44 8e e1 52 46 05 3a   Block  7: Disk 1, Row 1
1b 63 2e 59 e0 7a 66 62   Block  8: Disk 2, Row 1
04 dc 5d 8b 18 4f fa ea   Block  9: Disk 3, Row 1
00 00 00 00 00 00 00 00   Block 10: Disk 4, Row 1
00 00 00 00 00 00 00 00   Block 11: Disk 5, Row 1
4a 2e cb 69 6a ae 27 d7   Block 12: Disk 0, Row 2
9a 3b e3 19 92 01 ad 7e   Block 13: Disk 1, Row 2
8d 86 08 30 d4 74 e4 c5   Block 14: Disk 2, Row 2
e5 75 d9 f5 40 37 00 67   Block 15: Disk 3, Row 2
00 00 00 00 00 00 00 00   Block 16: Disk 4, Row 2
00 00 00 00 00 00 00 00   Block 17: Disk 5, Row 2
cd 8f 6e a4 ed b9 4e 2d   Block 18: Disk 0, Row 3
1b bd 9a a7 3f ee 76 34   Block 19: Disk 1, Row 3
00 00 00 00 00 00 00 00   Block 20: Disk 2, Row 3
00 00 00 00 00 00 00 00   Block 21: Disk 3, Row 3
00 00 00 00 00 00 00 00   Block 22: Disk 4, Row 3
00 00 00 00 00 00 00 00   Block 23: Disk 5, Row 3
UNIX> <font color=darkred><b></b></font>
</pre>

<hr>
<h2>Program #1: generate_random_data_file.c</h2>

The program <b>generate_random_data_file</b> has the following syntax:

<p><center><table border=3 cellpadding=3><td><pre>
generate_random_data_file n m s r blocksize seed
</pre></td></table></center><p>

It then generates a random data file in the above format.  The <b>seed</b> is a seed to
<b>srand48()</b>.  If set to -1, <b>srand48()</b> is seeed by <b>time(0)</b>.
The data file represents an unencoded SD codeword.  Thus, all of the blocks on 
disks <i>n-m</i> through <i>n-1</i> contain zeros.  Additionally, of the remaining 
blocks, the <i>s</i> blocks with the highest numbers also contain zeros.  
<p>
The file above (<b>example_6_2_2_4_data.txt</b>) was created with:

<pre>
UNIX> <font color=darkred><b>generate_random_data_file 6 2 2 4 8 0 > example_6_2_2_4_data.txt</b></font>
</pre>

As such, the blocks on disks 4 and 5 are all zero, as are blocks 20 and 21.

<hr>
<h2>Programs #2 and #3: fast_construction.c and general_construction.c</h2>

These programs create parity check matrices.  <b>fast_construction</b> takes
the following parameters:

<p><center><table border=3 cellpadding=3><td><pre>
fast_construction n m s r w a<sub>0</sub> ... a<sub>m+s-1</sub>
</pre></td></table></center><p>
The parameters are as follows:
<UL>
<LI> The first four are the same as <b>generate_random_data_file</b>. 
<LI> <i>w</i> is the symbol size (the word size of the Galois Field).
<LI> <i>a<sub>0</sub></i> through <i>a<sub>m+s-1</sub></i> are the coefficients.
</UL>
It then creates the parity check matrix using the methodology outlined in the FAST
paper.
For example, the following creates the parity check matrix for the code with 
<i>n=6</i>,
<i>m=2</i>,
<i>s=2</i>,
<i>r=4</i>,
<i>w=8</i> and coefficients 1, 42, 26 and 61.  This code is SD (this was determined in the
Monte Carlo searches of the FAST paper):

<pre>
UNIX> <font color=darkred><b>fast_construction 6 2 2 4 8 1 42 26 61 > example_fast_pcm.txt</b></font>
UNIX> <font color=darkred><b>cat example_fast_pcm.txt</b></font>
  1   1   1   1   1   1   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
  1  42  48 179 105  28   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
  0   0   0   0   0   0   1   1   1   1   1   1   0   0   0   0   0   0   0   0   0   0   0   0
  0   0   0   0   0   0 127 122 248   8  77 157   0   0   0   0   0   0   0   0   0   0   0   0
  0   0   0   0   0   0   0   0   0   0   0   0   1   1   1   1   1   1   0   0   0   0   0   0
  0   0   0   0   0   0   0   0   0   0   0   0 241 111 224 223 247 147   0   0   0   0   0   0
  0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   1   1   1   1   1   1
  0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0  64  82 156 219  95  83
  1  26  89 185 145  38  59  36  15 150  96 169  44 223 100 193  85   1  26  89 185 145  38  59
  1  61  56 241  41  59 182  97  53 205  44 242 110 115 184  26 120  10 143 173  36   7 179 168
UNIX> <font color=darkred><b></b></font>
</pre>


<p>
<b>general_construction</b> takes
the following parameters:

<p><center><table border=3 cellpadding=3><td><pre>
general_construction n m s r w x<sub>0</sub> y<sub>0</sub> x<sub>1</sub> y<sub>1</sub> .. x<sub>{m+s-1}</sub> y<sub>{m+s-1}</sub>
</pre></td></table></center><p>
The parameters are the same as <b>fast_construction</b>, only now you must specify the
elements of the <i>X</i> and <i>Y</i> sets.
It then creates the parity check matrix using the methodology outlined in [Plank-Blaum-2013].
The elements of <i>X</i> and <i>Y</i> may be negative -- they will be converted to a value
between 0 and <i>2<sup>w</sup>-2</i>.
<p>
As a first example, we can create the same parity check matrix as the FAST example by noting
that in <i>GF(2<sup>8</sup>)</i>, 
42 = 2<sup>142</sup>, 
26 = 2<sup>105</sup> and
61 = 2<sup>228</sup>.  So:

<pre>
UNIX> <font color=darkred><b>general_construction 6 2 2 4 8 0 0 142 142 105 105 228 228</b></font>
  1   1   1   1   1   1   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
  1  42  48 179 105  28   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
  0   0   0   0   0   0   1   1   1   1   1   1   0   0   0   0   0   0   0   0   0   0   0   0
  0   0   0   0   0   0 127 122 248   8  77 157   0   0   0   0   0   0   0   0   0   0   0   0
  0   0   0   0   0   0   0   0   0   0   0   0   1   1   1   1   1   1   0   0   0   0   0   0
  0   0   0   0   0   0   0   0   0   0   0   0 241 111 224 223 247 147   0   0   0   0   0   0
  0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   1   1   1   1   1   1
  0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0  64  82 156 219  95  83
  1  26  89 185 145  38  59  36  15 150  96 169  44 223 100 193  85   1  26  89 185 145  38  59
  1  61  56 241  41  59 182  97  53 205  44 242 110 115 184  26 120  10 143 173  36   7 179 168
UNIX> <font color=darkred><b></b></font>
</pre>

As a second example, in [Plank-Blaum-2013], we give a more general construction for codes where
<i>m=2</i> and <i>s=2</i>.  These are: <i>X = { 0, 0, 3, 2 }</i> and 
<i>Y = { 0, 1, -1, 2 }</i>:

<pre>
UNIX> <font color=darkred><b>general_construction 6 2 2 4 8 0 0  0 1  3 -1  2 2 > example_general_pcm.txt </b></font>
UNIX> <font color=darkred><b>cat example_general_pcm.txt</b></font>
  1   1   1   1   1   1   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
  1   2   4   8  16  32   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
  0   0   0   0   0   0   1   1   1   1   1   1   0   0   0   0   0   0   0   0   0   0   0   0
  0   0   0   0   0   0   1   2   4   8  16  32   0   0   0   0   0   0   0   0   0   0   0   0
  0   0   0   0   0   0   0   0   0   0   0   0   1   1   1   1   1   1   0   0   0   0   0   0
  0   0   0   0   0   0   0   0   0   0   0   0   1   2   4   8  16  32   0   0   0   0   0   0
  0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   1   1   1   1   1   1
  0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   1   2   4   8  16  32
  1 142  71 173 216 108  45 152  76  38  19 135  37 156  78  39 157 192  80  40  20  10   5 140
  1   4  16  64  29 116 205  19  76  45 180 234 143   6  24  96 157  78  37 148 106 181 238 159
UNIX> <font color=darkred><b></b></font>
</pre>

<hr>
<h2>Program #4: sd_codec.c</h2>

The program <b>sd_codec</b> takes quite a few command line arguments:

<p><center><table border=3 cellpadding=3><td><pre>
sd_codec n m s r w size IO(0|1) PCM Input Output d<sub>0</sub> .. d<sub>m-1</sub> s<sub>0</sub> .. s<sub>s-1</sub>
</pre></td></table></center><p>

The parameters are as follows:
<UL>
<LI> <i>n</i>, <i>m</i>, <i>s</i>, <i>r</i>, and
<i>size</i> are all the same as <b>generate_random_data_file</b>.
<LI> <i>w</i> is the symbol size (the word size of the Galois Field).
<LI> If <b>IO</b> equals 1, then it will read a file in the format specified above.
If <b>IO</b> equals 0, then it will generate a random stripe with the given parameters
(which is better for timing).
<LI> <b>PCM</b> is a file containing a parity check matrix for the code.
<LI> <b>Input</b> is a file containing the input, and
<b>Output</b> is a filename for the output
(when <b>IO</b> equals 0, both <b>input</b> and <b>Output</b> are ignored).
<LI> The <i>d<sub>i</sub></i> parameters specify erased disks, and the
the <i>s<sub>i</sub></i> parameters specify erased sectors.  
</UL>

<p>
<b>sd_codec</b> erases the <i>mr+s</i> blocks specified by <i>d<sub>i</sub></i> and 
<i>s<sub>i</sub></i>, and then decodes them using the SD code defined by the parity
check matrix.
It prints the entire stripe to the <b>Output</b> file in the same format as
as <b>generate_random_data</b>.

<p>For example, we can use <b>sd_codec</b> to encode the data from 
<b>example_6_2_2_4_data.txt</b> by specifying that
disks 4 and 5, plus blocks 20 and 21 have been erased.  We will do this twice -- once for
<b>example_fast_pcm.txt</b> and once for 
<b>example_general_pcm.txt</b>.  They will, of course, generate different coding data, but
each will be an SD code:

<pre>
UNIX> <font color=darkred><b>sd_codec 6 2 2 4 8 8 1 example_fast_pcm.txt example_6_2_2_4_data.txt example_6_2_2_4_encoded_fast.txt 4 5 20 21</b></font>
Timer: 0.000051
UNIX> <font color=darkred><b>cat example_6_2_2_4_encoded_fast.txt</b></font>
6e c0 0a 91 d0 f2 69 ae
ba b4 48 3b 0b 5c ea 66
83 81 c8 15 52 b9 29 7d
3a 7d 5a d4 63 29 8b 1e
f2 77 9e 08 c7 97 0a b2
9f ff 4e 63 2d a9 2b 19
bb 20 46 24 07 0d 8c 84
8b 44 8e e1 52 46 05 3a
1b 63 2e 59 e0 7a 66 62
04 dc 5d 8b 18 4f fa ea
06 5d c5 3f 32 b6 8d 9e
29 86 7e 28 9f c8 98 a8
4a 2e cb 69 6a ae 27 d7
9a 3b e3 19 92 01 ad 7e
8d 86 08 30 d4 74 e4 c5
e5 75 d9 f5 40 37 00 67
24 55 5c f1 99 dc ab 40
9c b3 a5 44 f5 30 c5 4b
cd 8f 6e a4 ed b9 4e 2d
1b bd 9a a7 3f ee 76 34
23 44 a9 cb 78 49 d9 11
36 b4 b8 c2 f9 14 71 ac
7d 83 b2 ac 66 c6 6a 5b
be 41 57 a6 35 cc fa ff
UNIX> <font color=darkred><b>sd_codec 6 2 2 4 8 8 1 example_general_pcm.txt example_6_2_2_4_data.txt example_6_2_2_4_encoded_general.txt 4 5 20 21</b></font>
Timer: 0.000064
UNIX> <font color=darkred><b>cat example_6_2_2_4_encoded_general.txt</b></font>
6e c0 0a 91 d0 f2 69 ae
ba b4 48 3b 0b 5c ea 66
83 81 c8 15 52 b9 29 7d
3a 7d 5a d4 63 29 8b 1e
ff 3a 78 b5 06 93 b1 6d
92 b2 a8 de ec ad 90 c6
bb 20 46 24 07 0d 8c 84
8b 44 8e e1 52 46 05 3a
1b 63 2e 59 e0 7a 66 62
04 dc 5d 8b 18 4f fa ea
83 70 28 cb 04 f8 1e 8b
ac ab 93 dc a9 86 0b bd
4a 2e cb 69 6a ae 27 d7
9a 3b e3 19 92 01 ad 7e
8d 86 08 30 d4 74 e4 c5
e5 75 d9 f5 40 37 00 67
89 3f f1 2f 6b 7a aa 1c
31 d9 08 9a 07 96 c4 17
cd 8f 6e a4 ed b9 4e 2d
1b bd 9a a7 3f ee 76 34
9c 27 79 ba 93 44 d5 46
4a 06 46 bf 6f eb 6e a2
52 23 b6 cb 2c 7a 57 6e
52 30 7d cd 02 82 d4 93
UNIX> <font color=darkred><b></b></font>
</pre>

You'll note that in each example, the original data blocks are unchanged.  
However, the coding blocks have been calculated according to the SD codes.

<hr>
<h2>Program #5: erase_data.c</h2>

The program <b>erase_data</b> takes the following command line arguments:

<p><center><table border=3 cellpadding=3><td><pre>
erase_data n m s r blocksize d<sub>0</sub> .. d<sub>m-1</sub> s<sub>0</sub> .. s<sub>s-1</sub>
</pre></td></table></center><p>

It reads a file in the proper format on standard input, and erases all of the blocks on 
disks
<i>d<sub>0</sub></i> to <i>d<sub>m-1</sub></i>, plus blocks <i>s<sub>0</sub></i> to <i>s<sub>s-1</sub></i>.  In the commands below, we erase disks 0 and 2 plus blocks 1 and 10 from the two 
encodded examples.

<pre>
UNIX> <font color=darkred><b>erase_data 6 2 2 4 8 0 2 1 10 < example_6_2_2_4_encoded_fast.txt > example_6_2_2_4_erased_fast.txt</b></font>
UNIX> <font color=darkred><b>cat example_6_2_2_4_erased_fast.txt</b></font>
00 00 00 00 00 00 00 00
00 00 00 00 00 00 00 00
00 00 00 00 00 00 00 00
3a 7d 5a d4 63 29 8b 1e
f2 77 9e 08 c7 97 0a b2
9f ff 4e 63 2d a9 2b 19
00 00 00 00 00 00 00 00
8b 44 8e e1 52 46 05 3a
00 00 00 00 00 00 00 00
04 dc 5d 8b 18 4f fa ea
00 00 00 00 00 00 00 00
29 86 7e 28 9f c8 98 a8
00 00 00 00 00 00 00 00
9a 3b e3 19 92 01 ad 7e
00 00 00 00 00 00 00 00
e5 75 d9 f5 40 37 00 67
24 55 5c f1 99 dc ab 40
9c b3 a5 44 f5 30 c5 4b
00 00 00 00 00 00 00 00
1b bd 9a a7 3f ee 76 34
00 00 00 00 00 00 00 00
36 b4 b8 c2 f9 14 71 ac
7d 83 b2 ac 66 c6 6a 5b
be 41 57 a6 35 cc fa ff
UNIX> <font color=darkred><b>erase_data 6 2 2 4 8 0 2 1 10 < example_6_2_2_4_encoded_general.txt > example_6_2_2_4_erased_general.txt</b></font>
UNIX> <font color=darkred><b>cat example_6_2_2_4_erased_general.txt</b></font>
00 00 00 00 00 00 00 00
00 00 00 00 00 00 00 00
00 00 00 00 00 00 00 00
3a 7d 5a d4 63 29 8b 1e
ff 3a 78 b5 06 93 b1 6d
92 b2 a8 de ec ad 90 c6
00 00 00 00 00 00 00 00
8b 44 8e e1 52 46 05 3a
00 00 00 00 00 00 00 00
04 dc 5d 8b 18 4f fa ea
00 00 00 00 00 00 00 00
ac ab 93 dc a9 86 0b bd
00 00 00 00 00 00 00 00
9a 3b e3 19 92 01 ad 7e
00 00 00 00 00 00 00 00
e5 75 d9 f5 40 37 00 67
89 3f f1 2f 6b 7a aa 1c
31 d9 08 9a 07 96 c4 17
00 00 00 00 00 00 00 00
1b bd 9a a7 3f ee 76 34
00 00 00 00 00 00 00 00
4a 06 46 bf 6f eb 6e a2
52 23 b6 cb 2c 7a 57 6e
52 30 7d cd 02 82 d4 93
UNIX> <font color=darkred><b></b></font>
</pre>

We can now use <b>sd_codec</b> to decode.  We'll do so in both cases to demonstrate that
the decoded data is identical to the encoded data!

<pre>
UNIX> <font color=darkred><b>sd_codec 6 2 2 4 8 8 1 example_fast_pcm.txt example_6_2_2_4_erased_fast.txt example_6_2_2_4_decoded_fast.txt 0 2 1 10</b></font>
Timer: 0.000071
UNIX> <font color=darkred><b>diff example_6_2_2_4_encoded_fast.txt example_6_2_2_4_decoded_fast.txt</b></font>
UNIX> <font color=darkred><b>sd_codec 6 2 2 4 8 8 1 example_general_pcm.txt example_6_2_2_4_erased_general.txt example_6_2_2_4_decoded_general.txt 0 2 1 10</b></font>
Timer: 0.000082
UNIX> <font color=darkred><b>diff example_6_2_2_4_encoded_general.txt example_6_2_2_4_decoded_general.txt</b></font>
UNIX> <font color=darkred><b></b></font>
</pre>

<hr>
<h2>Timing</h2>

You can set <b>IO</b> to zero and set the size large to time performance.  For example,
the following encodes the above code with 1 MB blocks:

<pre>
UNIX> <font color=darkred><b>sd_codec 6 2 2 4 8 1048576 0 example_general_pcm.txt - - 4 5 20 21</b></font>
Timer: 0.030266
UNIX>
</pre>

Since there are 14 MB of data in this stripe (24 total blocks, 10 devoted to coding), the
performance of coding is 14/0.031875 = 439 MB/s (this is on my 2.4 GHz Macbook Pro with 
iTunes, Safari, Photoshop and OpenOffice tooling along in the background).

<hr>
<h2>Caveats</h2>

You must specify <i>m</i> unique disk and <i>s</i> unique block erasures.  This is the 
hard decoding case.  
<p>
As mentioned above, <b>sd_codec</b> uses the alternate mapping of words to memory 
for <i>GF(2<sup>16</sup>)</i> and <i>GF(<sup>32</sup>)</i>.  I would recommend using 
the slower, default versions of the Galois Fields when you go about tweaking this code
for yourselves (just call <b>gf_init_easy(&amp;gfm, w)</b> for all values of <i>w</i>).
That, or read the "GF Complete" documentation on the alternate memory layouts.


<hr>
<h2>Coefficients of SD Codes from [Plank-Blaum-Hafner-2013]</h2>

The file <b>FAST-Coefficients.txt</a></b> contains coefficients
for valid SD constructions of codes using the FAST constructions.  
These are the ones responsible for Figure 4 in [Plank-Blaum-Hafner-2013].
They cover the following parameters:
<p>
<center>
<i>4 &le; n &le; 24</i>,<br>
<i>1 &le; m &le; 3</i>,<br>
<i>1 &le; s &le; 3</i>,<br>
<i>4 &le; r &le; 24</i>.
</center>
<p>

If we found valid coefficients for <i>w=8</i>, those are in the file.  Otherwise, if we found
valid coefficients for <i>w=16</i>, those are in the file.  Otherwise, we show the coefficients
for <i>w=32</i>.  Each line contains a different set of parameters in the following format:

<UL>
<LI> <i>n</i>, right justified to two characters.
<li> A space.
<LI> <i>m</i> - one character.
<li> A space.
<LI> <i>s</i> - one character.
<li> A space.
<LI> <i>r</i>, right justified to two characters.
<li> Four spaces.
<LI> <i>w</i>, right justified to two characters.
<li> Three spaces.
<LI> The <i>m+s</i> values of <i>a<sub>i</sub></i>, each separated by a space.
</UL>

Thus, for example, suppose you wanted to find the coefficients for <i>n = 16</i>, 
<i>m=3</i>, <i>s=2</i> and <i>r=9</i>.  You could do:

<pre>
UNIX> <font color=darkred><b>grep "^16 3 2  9" FAST-Coefficients.txt </b></font>
16 3 2  9    16   1 3561 27596 44295 42660
UNIX> <font color=darkred><b></b></font>
</pre>

That shows you that you must use <i>GF(2<sup>16</sup>)</i>, and the 5 coefficients are
1, 3561, 27596, 44295 and 42660.


<hr>
<h2>Coefficients of SD Codes from [Plank-Blaum-2013]</h2>

These are in the file <b>TOS-Coefficients.txt</b>.  The format is the same
as <b>FAST_Coefficients.txt</b>, except after the three spaces are 
<i>x<sub>0</sub></i>,
<i>y<sub>0</sub></i>,
<i>x<sub>1</sub></i>,
<i>y<sub>1</sub></i>,
...
<i>x<sub>m+s-1</sub></i>,
<i>y<sub>m+s-11</sub></i>.
There may be multiple SD codes for given values of <i>n, m, r</i> and <i>s</i>, 
with different values of <i>w</i>.  For example, there are three codes for 
<i>n=8</i>, <i>m=1</i>, <i>s=3</i> and <i>r=4</i>:

<pre>
UNIX> <font color=darkred><b>grep ' 8 1 3  4' TOS-Coefficients.txt</b></font>
 8 1 3  4  8   0 0 0 30 15 225 135 240
 8 1 3  4 16   0 0 24480 29835 28560 17850 32640 35700
 8 1 3  4 32   0 0 1 1 2 2 3 3 
UNIX> <font color=darkred><b></b></font>
</pre>

And here is an example of using <b>general_construction</b> to generate a parity check
matrix for the first of those codes:

<pre>
UNIX> <font color=darkred><b>general_construction 8 1 3 4 8   0 0 0 30 15 225 135 240</b></font>
  1   1   1   1   1   1   1   1   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
  0   0   0   0   0   0   0   0   1   1   1   1   1   1   1   1   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0
  0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   1   1   1   1   1   1   1   1   0   0   0   0   0   0   0   0
  0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   0   1   1   1   1   1   1   1   1
  1  96 185 223  59  85 150  89   1  96 185 223  59  85 150  89   1  96 185 223  59  85 150  89   1  96 185 223  59  85 150  89
  1  36 100 145 169  26  15 193  59 223 185  96   1  36 100 145  44  89 150  85  59 223 185  96  26  15 193  38  44  89 150  85
  1  44  36  89 100 150 145  85 185 193  96  38   1  44  36  89  59  26 223  15 185 193  96  38 150 145  85 169  59  26 223  15
UNIX> <font color=darkred><b></b></font>
</pre>

As we discover more SD codes, we will update <b>TOS-Coefficients</b> on bitbucket.

<hr>
<h2>Additional Reading</h2>

The main recommended reading for this work is [Plank-Blaum-2013], which is an extension
of the FAST paper [Plank-Blaum-Hafner-2013].
I would suggest [Plank-Greenan-Miller-2013], which describes fast Galois Field arithmetic
and the ``alternate mapping'' optimization.  
<p>
Four related codes that have different fault-tolerance properties but address
similar target areas are PMDS codes [Blaum-Hafner-Hetzler-2012], 
LRC codes [Huang-et-al-2012], Pyramid Codes [Huang-Chen-Li-2013], 
and Intradisk Redundancy [Dholakia-et-al-2008].

<hr>
<h2>Acknowledgements</h2>

I would like to acknowledge the help of Mario Blaum and Jim Hafner, my able co-conspirators
in the SD FAST paper.  I would also like to acknowledge Kevin Greenan, Ethan Miller and
Will Houston, my able co-conspirators in GF-Complete.  
<p>
This material is based upon work supported by the National Science
Foundation under grant CSR-1016636 and its REU supplements.
In particular, Will, Adam and Andrew were all funded by REU's for their part in this
work.  
<p>
Finally, the I would like to acknowledge the support of IBM's Faculty Award program, 
which granted me an award in December, 2012.

<hr>
<h2>References</h2>

<UL>
<p><LI> M. Blaum, J. L. Hafner and S. Hetzler, <i>"Partial-MDS Codes and
Their Application to RAID Type of Architectures,"</i> IBM Research
Report RJ10498 (ALM1202-001), February, 2012. (A version of this paper will appear in
<b>IEEE Transactions on Information Theory</b>).  This paper is heavy on theory, but
uses the same encoding and decoding methodology as SD codes.

<p><LI> [Huang-et-al-2012] C. Huang, H. Simitci, Y. Xu, A. Ogus, B. Calder, P. Gopalan, J.
Li and S. Yekhanin, <i>"Erasure Coding in Windows Azure Storage,"</i>
<b>USENIX Annual Technical Conference</b>, Boston, June, 2012.

<p><LI> [Huang-Chen-Li-2013] C. Huang, M. Chen and J. Li, <i>"Pyramid Codes: Flexible Schemes
to Trade Space for Access Efficiency in Reliable Data Storage
Systems,"</i> <b>ACM Transactions on Storage</b>, 9(1), Feb, 2013.

<p><LI> [Dholakia-et-al-2008] A. Dholakia, E. Eleftheriou, X. Y. Hu, I. Iliadis, J. Menon and
K. K. Rao, <i>"A New Intra-disk Redundancy Scheme for
High-Reliability RAID Storage Systems in the Presence of
Unrecoverable Errors,"</i> <b>ACM Transactions on Storage</b>, 4(1),
May, 2008, pp. 1-42.

<p><LI> [Plank-Blaum-2013] J. S. Plank and M. Blaum, <i>"Sector-Disk (SD) Erasure Codes for Mixed Failure Modes in RAID Systems</i>", Technical Report UT-CS-13-708, University of
Tennessee, May, 2013.
<a href=http://web.eecs.utk.edu/~plank/plank/papers/CS-13-708.html>http://web.eecs.utk.edu/~plank/plank/papers/CS-13-708.html</a>

<p><LI> [Plank-Blaum-Hafner-2013] J. S. Plank, M. Blaum and J. L. Hafner, <i>"SD Codes: Erasure
Codes Designed for How Storage Systems Really Fail,"</i>
<b>FAST-2013: 11th Usenix Conference on File and StorageTechnologies</b>, San Jose, February, 2013.
<a href=http://web.eecs.utk.edu/~plank/plank/papers/FAST-2013-SD.html>http://web.eecs.utk.edu/~plank/plank/papers/FAST-2013-SD.html</a>.

<p><LI> [Plank-Greenan-Miller-2013] J. S. Plank, K. M. Greenan and E. L. Miller, <i>"Screaming Fast
Galois Field Arithmetic Using Intel SIMD Instructions,"</i><b>FAST-2013: 11th Usenix Conference on File and Storage
Technologies</b>, San Jose, February, 2013. <a href=http://web.eecs.utk.edu/~plank/plank/papers/FAST-2013-GF.html>http://web.eecs.utk.edu/~plank/plank/papers/FAST-2013-GF.html</a>.

<p><LI> [Plank-Greenan-Miller-Houston-2013] J. S. Plank, K. M. Greenan, E. L. Miller and W. B. Houston,
<i>"GF-Complete: A Comprehensive Open Source Library for Galois Field
Arithmetic,"</i> Technical Report UT-CS-13-703, University of
Tennessee, January, 2013.
<a href=http://web.eecs.utk.edu/~plank/plank/papers/CS-13-703.html>http://web.eecs.utk.edu/~plank/plank/papers/CS-13-703.html</a>

</UL>
