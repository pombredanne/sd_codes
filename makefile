# No fancy makefile here -- just keeping it simple.
# You'll need to download gf_complete, compile it, 
# and then copy over gf_complete.h and gf_complete.a

all: sd_codec fast_construction generate_random_data_file erase_data \
      general_construction
clean: 
	rm -f *.o sd_codec generate_random_data_file erase_data \
               fast_construction general_construction

sd_codec: sd_codec.c gf_complete.h gf_complete.a
	gcc -O3 -o sd_codec sd_codec.c gf_complete.a

fast_construction: fast_construction.c gf_complete.h gf_complete.a
	gcc -O3 -o fast_construction fast_construction.c gf_complete.a

general_construction: general_construction.c gf_complete.h gf_complete.a
	gcc -O3 -o general_construction general_construction.c gf_complete.a

generate_random_data_file: generate_random_data_file.c
	gcc -O3 -o generate_random_data_file generate_random_data_file.c

erase_data: erase_data.c
	gcc -O3 -o erase_data erase_data.c

