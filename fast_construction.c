#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include "gf_complete.h"

#define talloc(type, num) (type *) malloc(sizeof(type)*(num))

void usage(char *s)
{
  fprintf(stderr, "usage: fast_construction n m s r w a_0 ... a_m+s-1 - produces the PCM\n");
  if (s != NULL) fprintf(stderr, "%s\n", s);
  exit(1);
}


void print_matrix(int *m, int rows, int cols, int w)
{
  int i, j;
  int fw;
  char s[30];
  unsigned int w2;

  if (w == 32) {
    fw = 10;
  } else {
    w2 = (1 << w);
    sprintf(s, "%u", w2-1);
    fw = strlen(s);
  }

  for (i = 0; i < rows; i++) {
    for (j = 0; j < cols; j++) {
      if (j != 0) printf(" ");
      printf("%*u", fw, m[i*cols+j]);
    }
    printf("\n");
  }
}

int main(int argc, char **argv)
{
  int i, j, n, m, r, s, w;
  int *matrix;
  int alpha, k, coef, symbol, l;
  int *alphas, alpha_counter;
  int pp;
  gf_t gf_s;

  if (argc <= 6) usage(NULL);
  if (sscanf(argv[1], "%d", &n) == 0 || n <= 0) usage("Bad n");
  if (sscanf(argv[2], "%d", &m) == 0 || m <= 0) usage("Bad m");
  if (sscanf(argv[3], "%d", &s) == 0 || s <= 0) usage("Bad s");
  if (sscanf(argv[4], "%d", &r) == 0 || r <= 0) usage("Bad r");
  if (sscanf(argv[5], "%d", &w) == 0 || w <= 0) usage("Bad w");
  pp = 0;
  sscanf(argv[5], "%d-%o", &i, &pp);

  if (argc != 6 + m+s) usage("Wrong number of arguments");

  alphas = talloc(int, m+s);
  for (i = 0; i < m+s; i++) {
    if (sscanf(argv[6+i], "%d", alphas+i) == 0 || alphas[i] <= 0) usage("Bad ai");
  }

  matrix = talloc(int, n*r*(m*r+s));
  bzero(matrix, sizeof(int)*n*r*(m*r+s));

  if (pp == 0) {
    if (w == 32) {      
      if (gf_init_hard(&gf_s, w, GF_MULT_SPLIT_TABLE, GF_REGION_DEFAULT, GF_DIVIDE_EUCLID, 0, 8, 8, 
             NULL, NULL) == 0) {
        fprintf(stderr, "Couldn't open GF\n");
        exit(1);
      }
    } else {
      if (gf_init_easy(&gf_s, w) == 0) {
        fprintf(stderr, "Couldn't open GF\n");
        exit(1);
      }
    }
  } else {
    if (gf_init_hard(&gf_s, w, GF_MULT_SHIFT, GF_REGION_DEFAULT,
                     GF_DIVIDE_EUCLID, pp, 0, 0, NULL, NULL) == 0) {
      fprintf(stderr, "Couldn't open GF\n");
      exit(1);
    }
  }

   alpha_counter = 0;
  for (i = 0; i < m; i++) {
    coef = 1;
    for (k = 0; k < r; k++) {
      for (j = 0; j < n; j++) {
        matrix[(k*m+i)*(r*n)+(k*n)+j] = coef;
        coef = gf_s.multiply.w32(&gf_s, coef, alphas[alpha_counter]);
      }
    }
    alpha_counter++;
  }
  for (i = 0; i < s; i++) {
    coef = 1;
    for (j = 0; j < n*r; j++) {
      matrix[(n*r)*(m*r+i)+j] = coef;
      coef = gf_s.multiply.w32(&gf_s, coef, alphas[alpha_counter]);
    }
    alpha_counter++;
  }
  print_matrix(matrix, m*r+s, n*r, w);
  exit(0);
} 
